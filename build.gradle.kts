import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    id("org.jetbrains.kotlin.jvm") version "1.3.50" apply false
}

group = "vanadis"
version = "1.0.0"

allprojects {
    repositories {
        jcenter()
    }
}

subprojects {

    tasks.withType<KotlinCompile>().configureEach {

        println("Configuring $name in project ${project.name}...")

        kotlinOptions {

            jvmTarget = "11"

            // strict - convert nullability annotations into errors
            // enable - will stay tham as is
            // ignore - will skip them
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }

}
