/*
 * The settings file is used to specify which projects to include in your build.
 *
 * Detailed information about configuring a multi-project build in Gradle can be found
 * in the user manual at https://docs.gradle.org/5.5/userguide/multi_project_builds.html
 */

rootProject.name = "vanadis"

fun isModulesPresent(vararg moduleNames: String): Boolean {

    moduleNames.forEach { moduleName ->
        val moduleConfigFile = file("./$moduleName/build.gradle.kts")

        if (!moduleConfigFile.exists()) {
            return false
        }
    }

    return true
}

/**
 * Required section
 */

include(":common:annotation")
include(":common:logger")

include(":vanadis:core")

/**
 * Optional section
 */

if (isModulesPresent("vanadis/discord")) {
    include(":vanadis:discord")

    if (isModulesPresent("bot/music")) {
        include(":bot:music")
    }


    if (isModulesPresent("bot/moderator")) {
        include(":bot:moderator")
    }
}

if (isModulesPresent("dashboard/frontend", "dashboard/frontend-dashboard", "dashboard/backend")) {
    include(":dashboard:frontend-dashboard")
    include(":dashboard:frontend")
    include(":dashboard:backend")
}
